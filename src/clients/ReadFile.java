package clients;

import java.io.File;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadFile {
	
	private Scanner f;
	private int timeLimit;
	private int maxProcessingTime;
	private int minProcessingTime;
	private int numberOfServers;
	private int numberOfClients;
	private int maxArrivalTime;
	private int minArrivalTime;
	 
	public ReadFile(String fileName) {
		try {
			f = new Scanner(new File(fileName));
		}catch(Exception e){
			System.out.println("File not found");
		}
		readFile();
		f.close();
	}
	
	private void readFile() {
		String a=null, b=null, c=null, d = null, e = null;
		while(f.hasNext()) {
			 a = f.next();
			 b = f.next();
			 c = f.next();
			 d = f.next();
			 e = f.next();
		}
		numberOfClients=Integer.parseInt(a);
		numberOfServers=Integer.parseInt(b);
		timeLimit=Integer.parseInt(c);
		separate(d, minArrivalTime, maxArrivalTime, 0);
		separate(e, minProcessingTime, maxProcessingTime, 1);
		
	}
	
	public int getTimeLimit() {
		return timeLimit;
	}

	public int getMaxProcessingTime() {
		return maxProcessingTime;
	}

	public int getMinProcessingTime() {
		return minProcessingTime;
	}

	public int getNumberOfServers() {
		return numberOfServers;
	}

	public int getNumberOfClients() {
		return numberOfClients;
	}

	public int getMaxArrivalTime() {
		return maxArrivalTime;
	}

	public int getMinArrivalTime() {
		return minArrivalTime;
	}

	private void separate(String minMaxTime, int minTime, int maxTime, int contor ) {
	     int count = 0;
	     Pattern polyFormat = Pattern.compile("\\d+");
	     Matcher m = polyFormat.matcher(minMaxTime);
	     while (m.find()){
				if (m.group().length() != 0){
					if(count == 0) {
						minTime=Integer.parseInt(m.group().trim());
						count++;
					}
				else {
					maxTime=Integer.parseInt(m.group().trim());
				}
				}
		if (contor==0) {
			this.minArrivalTime=minTime;
			this.maxArrivalTime=maxTime;
		}else {
			this.minProcessingTime=minTime;
			this.maxProcessingTime=maxTime;
		}
	}
	}
}
