package clients;

import java.util.Collections;
import java.util.List;
import java.util.Comparator;

public class ConcreteStrategyTime implements Strategy{

	@Override
	public void addTask(List<Server> servers, Task t) {
			int min=Integer.MAX_VALUE, ok=0;
			for(Server s : servers) {
				if(s.getWaitingPeriod().get()<min) {
					min=s.getWaitingPeriod().get();
				}
			}
			for(Server s : servers) {
				if(s.getWaitingPeriod().get()==min && ok==0) {
					s.addTask(t);
					ok=1;
				}
			}
		
	}

}
