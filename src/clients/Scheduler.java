package clients;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Scheduler {
	
	private List<Server> servers=Collections.synchronizedList(new ArrayList<Server>());
	private int maxNoServers;
	private int maxTasksPerServer;
	private Strategy strategy;
	
	public Scheduler(int maxNoServers, int maxTasksPerServer) {
		this.maxNoServers=maxNoServers;
		this.maxTasksPerServer=maxTasksPerServer;
		for(int i=0; i<maxNoServers; i++) {
			Server server = new Server();
			servers.add(server);
		}
			
	}
	
	public void changeStrategy(SelectionPolicy policy) {
		strategy = new ConcreteStrategyTime();
	}
	
	public void dispatchTask(Task t) {
		strategy.addTask(servers, t);
	}
	
	public List<Server> getServers(){
		return servers;
	}

}
