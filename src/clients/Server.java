package clients;


import java.util.Formatter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
	private BlockingQueue<Task> tasks= new PriorityBlockingQueue<Task>();
	private AtomicInteger waitingPeriod= new AtomicInteger(0);
	private Task[] arrTasks = null;
	private int nrServer;
	private Task processingTask = new Task();
	public int getNrServer() {
		return nrServer;
	}

	public void setNrServer(int nrServer) {
		this.nrServer = nrServer;
	}

	
	public Server() {
		
	}
	
	public void addTask(Task newTask) {
		
		tasks.add(newTask);
		waitingPeriod.addAndGet(newTask.getProcessingTime());
	}
	
	public void run() {
		while(true) {
			Task task = new Task();
			try {
			task = tasks.poll();
			this.setProcessingTask(task);
			int count=task.getProcessingTime();
			try {
				while(count!=0) {
				waitingPeriod.decrementAndGet();
				count--;
				Thread.sleep(1000);
				}
			} catch (InterruptedException e) {
				System.out.println("s-a intrerupt");
			}
		}catch(NullPointerException e) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				System.out.println("s-a intrerupt");
			}
			continue;
		}
	}
	}
	
	public void getTasks(Formatter f) {
		
		for(Task t : tasks) {
			f.format("%s%s%s%s%s%s%s", "(", t.getId(), " ", t.getArrivalTime(), " ", t.getProcessingTime(), "); ");

		}
		f.format("\n");
		
	}
	
	public void setProcessingTask(Task t) {
		this.processingTask=t;
		
	}
	
	public Task getProcessingTask() {
		return this.processingTask;
	}
	
	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
		
	}


}
