package clients;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Random;

public class SimulationManager implements Runnable {
	
	private int timeLimit;
	private int maxProcessingTime;
	private int minProcessingTime;
	private int numberOfServers;
	private int numberOfClients;
	public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;
	private int maxArrivalTime;
	private int minArrivalTime;
	private int count=1;
	private List<Server> servers=Collections.synchronizedList(new ArrayList<Server>());
	private Formatter f;
	private Scheduler scheduler;
	
	private List<Task> generatedTasks = new ArrayList<Task>();
	
	public SimulationManager(int nrOfClients, int nrOfServers, int tLimit, int minArrTime, int maxArrTime, int minProcTime, int maxProcTime, String fileName) {
		setBounds( nrOfClients,  nrOfServers,  tLimit,  minArrTime,  maxArrTime,  minProcTime, maxProcTime);
		openFile(fileName);
		scheduler = new Scheduler(numberOfServers, numberOfClients);
		scheduler.changeStrategy(selectionPolicy);
		servers = scheduler.getServers();
		generateNRandomTasks();
		Thread []thread = new Thread[numberOfServers];
		int i=0;
		for(Server s : servers) {
			thread[i] = new Thread(s);
			thread[i].start();
			s.setNrServer(i);
			i++;
		}
	}
	
	public void openFile(String fileName) {
		try {
			this.f = new Formatter(fileName);
		}catch(Exception e){
			System.out.println("Fisierul nu a putut fi deschis");
		}
	}
	
	private void setBounds(int numberOfClients, int numberOfServers, int timeLimit, int minArrivalTime, int maxArrivalTime, int minProcessingTime, int maxProcessingTime) {
		this.numberOfClients=numberOfClients;
		this.numberOfServers=numberOfServers;
		this.timeLimit=timeLimit;
		this.minArrivalTime=minArrivalTime;
		this.maxArrivalTime=maxArrivalTime;
		this.minProcessingTime=minProcessingTime;
		this.maxProcessingTime=maxProcessingTime;
	}
	
	private void generateNRandomTasks() {
		for(int i=0; i<numberOfClients; i++) {
		Random rand = new Random();
		Task task = new Task();
		task.setProcessingTime(rand.nextInt(maxProcessingTime-minProcessingTime+1)+minProcessingTime);
		task.setArrivalTime(rand.nextInt(maxArrivalTime-minArrivalTime+1)+minArrivalTime);
		generatedTasks.add(task);
		}
		Collections.sort(generatedTasks);
		
		for(Task ts : generatedTasks) {
			ts.setId(count);
			count++;
		}
	}
	
	public void run() {
		int currentTime=0;
		while(currentTime<timeLimit) {
			int i=0, j=0;
			Task[] processingTasks = new Task[numberOfClients];
			Task[] t = new Task[numberOfClients];
			f.format("%s%s%s", "Time :", currentTime, "\n");
			for(Task g : generatedTasks) {
				if(g.getArrivalTime()==currentTime) {
					scheduler.dispatchTask(g);
					t[j]=g;
					j++;
				}
			}
			for(int contor=0; contor<=j; contor++) {
				generatedTasks.remove(t[contor]);
			}
			f.format("%s", "Waiting Clients: ");
			generatedTasks.forEach((n) -> f.format("%s%s%s%s%s%s%s", "(", n.getId(), " ", n.getArrivalTime(), " ", n.getProcessingTime(), "); "));
			f.format("\n");
			for(Server s : servers) {
				displayTasks(s, i, processingTasks);
				i++;
			}
			currentTime++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("interrupted");
			}
			f.format("\n");
		}
		f.close();
	}
	
	private void displayTasks(Server s, int i, Task[] processingTasks) {
		if(s.getProcessingTask()!=null && s.getProcessingTask().getProcessingTime()!=1) {
			processingTasks[i] = new Task();
			processingTasks[i]=s.getProcessingTask();
			processingTasks[i].setProcessingTime(processingTasks[i].getProcessingTime()-1);
			f.format("%s%s%s", "Server ", i, ": ");
			f.format("%s%s%s%s%s%s%s", "(", processingTasks[i].getId(), " ", processingTasks[i].getArrivalTime(), " ", processingTasks[i].getProcessingTime(), "); ");

			}else if(s.getWaitingPeriod().get()==0){
				f.format("%s%s%s", "Server ", i, ": closed; ");
			}else {
				f.format("%s%s%s", "Server ", i, ": ");
			}
			s.getTasks(f);
	}
	
	public static void main(String[] args) {
		
		ReadFile f = new ReadFile(args[0]);
		SimulationManager gen = new SimulationManager(f.getNumberOfClients(), f.getNumberOfServers(), f.getTimeLimit(), f.getMinArrivalTime(), f.getMaxArrivalTime(), f.getMinProcessingTime(), f.getMaxProcessingTime(), args[1]);
		Thread t = new Thread(gen);
		t.start();
		
	}

}
