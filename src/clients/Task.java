package clients;

public class Task implements Comparable<Task> {
	private int arrivalTime;
	private int processingTime;
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getProcessingTime() {
		return processingTime;
	}
	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}
	public int compareTo(Task taskToCompare) {
	    if (this.arrivalTime < taskToCompare.getArrivalTime())
	        return -1;
	    else if (this.arrivalTime == taskToCompare.getArrivalTime())
	        return 0;
	    else
	        return 1;
	}
}
